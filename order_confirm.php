<?php
session_start();

if(isset($_POST["confirm"])) {
  $_SESSION["address"] = htmlspecialchars($_POST["addr21"], ENT_QUOTES);
  $_SESSION["pay"] = htmlspecialchars($_POST["pay"], ENT_QUOTES);

  $dns = ("mysql:host=localhost;dbname=ec_site;charaset=utf8");
  $user = "root";
  $password = "root"; 
  
  try{
    $db = new PDO($dns, $user, $password);
    $db->beginTransaction();

    //保存処理 orders table
    $user_id = $_SESSION["user_id"];
    $address = $_SESSION["address"];
    $total = $_SESSION["total"];

    $sql = "INSERT INTO orders(id, user_id, address, total) VALUES(NULL, :user_id, :address, :total)";
    
    $stmt = $db->prepare($sql);
    $stmt->bindParam(":user_id", $user_id, PDO::PARAM_STR);
    $stmt->bindParam(":address", $address, PDO::PARAM_STR);
    $stmt->bindParam(":total", $total, PDO::PARAM_STR);
    $stmt->execute();

    //保存処理 order_detail
    $order_id = $db->lastInsertId("id");
    foreach($_SESSION["cart"] as $item){
      $product_id = $item["id"];
      $amount = $item["amount"];

      $sql = "INSERT INTO order_detail(id, order_id, product_id, amount)VALUES(NULL, :order_id , :product_id, :amount)";
      $stmt = $db->prepare($sql);
      $stmt->bindParam(":order_id", $order_id, PDO::PARAM_STR);
      $stmt->bindParam(":product_id", $product_id, PDO::PARAM_STR);
      $stmt->bindParam(":amount", $amount, PDO::PARAM_STR);
      $stmt->execute();

    }

    $db->commit();
    $_SESSION["cart"] = [];
  } catch (PDOException $e){
    $db->rollBack();
    echo "接続失敗:" .$e->getMessage(). "\n";
    exit();
  }
  
  $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  require "mail_test.php";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>注文確認画面</title>
  <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
</head>
<body>
  <input type="button" value="戻る" onclick="history.back()">
  <table border=1>
    <tr>
      <th>商品名</th>
      <th>商品画像</th>
      <th>紹介文</th>
      <th>価格</th>
      <th>数量</th>
    </tr>
    <?php foreach($_SESSION['cart'] as $item) { ?>
      <tr>
        <td><?php echo $item["item"] ?></td>
        <td><img src="<?php echo $item['image'] ?>"></td>
        <td><?php echo $item["intro"] ?></td>
        <td><?php echo $item["price"] ?></td>
        <td><?php echo $item["amount"] ?></td>
      </tr>
    <?php } ?>  
  </table>
  <p>合計金額:<?php echo $_SESSION["total"]. "円"; ?></p>
  
  <form action="order_confirm.php" method="post">

    <p>郵便番号</p>
    <input type="text" name="zip21" size="4" maxlength="3"> － <input type="text" name="zip22" size="5" maxlength="4" onKeyUp="AjaxZip3.zip2addr('zip21','zip22','addr21','addr21');">

    <p>住所</p> 
    <input type="text" name="addr21" size="40">
    
    <p>支払い方法</p>
    <select name="pay">
      <option value="daibiki">代引き</option>
    </select>
    <br>
    <input type="submit" name="confirm" value="確定する">
  </form>


</body>
</html>