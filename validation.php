<?php

class register {

  public function user_register(){
    $errors = [];
    $name = $_POST["name"];
    $post_number = $_POST["zip21"];
    $address = $_POST["addr21"];
    $email = $_POST["email"];
    $password = $_POST["password"];

    if($name == ""){
      $errors["name"] = "氏名を入力してください";
    }
    if($post_number == ""){
      $errors["post_number"] = "住所を入力してください";
    }
    if($address == ""){
      $errors["address"] = "住所を入力してください";
    }
    if($email == ""){
      $errors["email"] = "メールアドレスを入力してください";
    }
    if($password == ""){
      $errors["password"] = "パスワードを入力してください";
    }
    return $errors;
  }

  public function product_register() {
    $product_errors = [];
    $item = $_POST["item"];
    $image = $_FILES["image"]["tmp_name"];
    $intro = $_POST["intro"];
    $price = $_POST["price"];

    if($item == "") {
      $product_errors["item"] = "商品名を入力してください";
    }
    if($image == "") {
      $product_errors["image"] = "商品画像を選択してください";
    }    
    if($intro == "") {
      $product_errors["intro"] = "紹介文を入力してください";
    }
    if($price == "") {
      $product_errors["price"] = "価格を入力してください";
    }
    return $product_errors;
  }

  public function comment_register() {
    $comment_errors = [];
    $nickname = $_POST["nickname"];
    $comment = $_POST["comment"];

    if($nickname == "") {
      $comment_errors["nickname"] = "ニックネームを入力してください";
      // var_dump($comment_errors["nickname"]);
    }
    if($comment == "") {
      $comment_errors["comment"] = "口コミを入力してください";
      // var_dump($comment_errors["comment"]);exit;
    }
    return $comment_errors;
  }
}

class login {

  public function user_login() {
    $login_valid = [];
    $email = $_POST["email"];
    $password = $_POST["password"];

    if($email == ""){
      $login_valid["email"] = "メールアドレスを入力してください";
    }
    if($password == ""){
      $login_valid["password"] = "パスワードを入力してください";
    }
    return $login_valid;
  }
}
?>