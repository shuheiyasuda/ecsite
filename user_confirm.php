<?php
session_start();

if(isset($_POST["submit"])) {
  $dns = ("mysql:host=localhost;dbname=ec_site;charaset=utf8");
  $user = "root";
  $password = "root"; 

  try{
    $db = new PDO($dns, $user, $password);
  } catch (PDOException $e){
    echo "接続失敗:" .$e->getMessage(). "\n";
    exit();
  }

  $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //パスワードハッシュ化
    $pass_hash = password_hash($_SESSION["password"], PASSWORD_DEFAULT);

    $userName = $_SESSION["name"];
    $userAddress = $_SESSION["address"];
    $userEmail = $_SESSION["email"];
    $userPassword = $pass_hash;

    $sql = "INSERT INTO users(id, name, address, email, password)
            VALUES(NULL, :userName, :userAddress, :userEmail, :userPassword)";

    $stmt = $db->prepare($sql);
    $stmt->bindParam(':userName', $userName, PDO::PARAM_STR);
    $stmt->bindParam(':userAddress', $userAddress, PDO::PARAM_STR);
    $stmt->bindParam(':userEmail', $userEmail, PDO::PARAM_STR);
    $stmt->bindParam(':userPassword', $userPassword, PDO::PARAM_STR);
    $stmt->execute();

  header("Location:user_complete.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ユーザー登録確認</title>
</head>
<body>
  <form action="user_confirm.php" method="post">
    <p>氏名</p>
      <?php echo "$_SESSION[name]"; ?>
    <p>住所</p> 
      <?php echo "$_SESSION[address]"; ?>
    <p>メールアドレス</p>
      <?php echo "$_SESSION[email]"; ?>
    <p>パスワード</p>
      <?php echo "$_SESSION[password]"; ?>
    <br>
    <input type="button" value="戻る" name="btn_back" onclick="history.back()">
    <input type="submit" value="登録" name="submit">
  </form>
</body>
</html>