<?php
mb_language("japanese");
mb_internal_encoding("UTF-8");

require './PHPmailer-master/src/PHPmailer.php';
require './PHPmailer-master/src/SMTP.php';
require './PHPmailer-master/src/POP3.php';
require './PHPmailer-master/src/Exception.php';
require './PHPmailer-master/src/OAuth.php';
require './PHPmailer-master/language/phpmailer.lang-ja.php';

//phpmailer使用宣言
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//SMTPの設定
$mailer = new PHPMailer();
$mailer->IsSMTP();
$mailer->Host = 'smtp.gmail.com';
$mailer->CharSet = 'utf-8';
$mailer->SMTPAuth = TRUE;
$mailer->Username = 'yasu.canes07@gmail.com';
$mailer->Password = 'yasuda1234';
$mailer->IsHTML(false);
$mailer->SMTPSecure = 'tls';
$mailer->Port = 587;
$mailer->SMTPDebug = 2;

//メール本体
$to = "yasu.canes07@gmail.com";
$mailer->From     = 'yasu.canes07@i.softbank.jp';
$mailer->SetFrom('yasu.canes07@i.softbank.jp');
$mailer->FromName = mb_convert_encoding('ECサイト', 'UTF-8', 'AUTO');
$mailer->Subject  = mb_convert_encoding('購入完了のお知らせ', 'UTF-8', 'AUTO');
$mailer->Body     = mb_convert_encoding('購入完了しました', 'UTF-8', 'AUTO');
$mailer->AddAddress($to);

//送信する
if($mailer->Send()){
  header("Location:order_complete.php");
} else{
  echo "送信に失敗しました".$mailer->ErrorInfo;
  echo "<a href='product_list.php'>商品一覧画面へ</a>";
}

?>