<?php
  session_start();

  if(isset($_POST["submit"])) {
    $dns = ("mysql:host=localhost;dbname=ec_site;charaset=utf8");
    $user = "root";
    $password = "root"; 
  
    try{
      $db = new PDO($dns, $user, $password);
    } catch (PDOException $e){
      echo "接続失敗:" .$e->getMessage(). "\n";
      exit();
    }
  
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $itemName = $_SESSION["item"];
    $itemImage = $_SESSION["image"];
    $itemIntro = $_SESSION["intro"];
    $itemPrice = $_SESSION["price"];

    $sql = "INSERT INTO products(id, item, image, intro, price)
            VALUES(NULL, :itemName, :itemImage, :itemIntro, :itemPrice)";

    $stmt = $db->prepare($sql);
    $stmt->bindParam(':itemName', $itemName, PDO::PARAM_STR);
    $stmt->bindParam(':itemImage', $itemImage, PDO::PARAM_STR);
    $stmt->bindParam(':itemIntro', $itemIntro, PDO::PARAM_STR);
    $stmt->bindParam(':itemPrice', $itemPrice, PDO::PARAM_STR);
    $stmt->execute();

    header("Location:product_complete.php");
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>商品登録確認画面</title>
</head>
<body>
  <form action="product_confirm.php" method="post" enctype="multipart/form-data">
    <p>商品名</p>
    <?php echo $_SESSION["item"]; ?>
    <p>商品画像</p>
    <img src="<?php echo $_SESSION['image']; ?>">
    <p>紹介文</p>
    <?php echo $_SESSION["intro"]; ?>
    <p>価格</p>
    <?php echo $_SESSION["price"]; ?>
    <br>
    <input type="button" name="btn_back" value="戻る" onclick="history.back()">
    <input type="submit" name="submit" value="送信">
  </form>
</body>
</html>