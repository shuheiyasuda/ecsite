<?php

require "validation.php";

session_start();
if(isset($_POST["submit"])) {
  $cls = new register();
  $errors = $cls->comment_register();

  if(empty($errors)){
    $dns = ("mysql:host=localhost;dbname=ec_site;charaset=utf8");
    $user = "root";
    $password = "root"; 

    try{
      $db = new PDO($dns, $user, $password);
    } catch (PDOException $e){
      echo "接続失敗:" .$e->getMessage(). "\n";
      exit();
    }

    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //ユーザー検索
    $user_id = $_SESSION["user_id"];

    $sql = "SELECT * FROM users WHERE id = :id";

    $stmt = $db->prepare($sql);
    $stmt->bindParam(":id", $user_id, PDO::PARAM_STR);
    $stmt->execute();
    $user = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($user as $user) {
      $user_id = $user["id"];
    }

    //商品検索
    $item_id = $_SESSION["item_id"];
    $sql = "SELECT * FROM products WHERE id = :id";

    $stmt = $db->prepare($sql);
    $stmt->bindParam(":id", $item_id, PDO::PARAM_STR);
    $stmt->execute();
    $item = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($item as $item) {
      $item_id = $item["id"];
    }

    $nickname = htmlspecialchars($_POST["nickname"]);
    $comment = htmlspecialchars($_POST["comment"]);

    //comment tableへinsert
    $sql = "INSERT INTO comments(id, nickname, comment, user_id, product_id) VALUES (NULL, :nickname, :comment, :user_id, :product_id)";
    $stmt = $db->prepare($sql);
    $stmt->bindParam(":nickname", $nickname, PDO::PARAM_STR);
    $stmt->bindParam(":comment", $comment, PDO::PARAM_STR);
    $stmt->bindParam(":user_id", $user_id, PDO::PARAM_STR);
    $stmt->bindParam(":product_id", $item["id"], PDO::PARAM_STR);
    $stmt->execute();

    header("Location:product_detail.php");
  }
}
?>




<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>口コミ登録画面</title>
</head>
<body>
  <form action="" method="post">
    ニックネーム
    <br>
    <input type="text" name="nickname" value="<?php if(isset($_POST['nickname'])) {echo $_POST['nickname'];} ?>">
    <?php if(isset($errors["nickname"])) { echo $errors["nickname"];} ?>
    <br>
    口コミ
    <br>
    <textarea name="comment" cols="30" rows="10"><?php if(isset($_POST['comment'])) {echo $_POST['comment'];} ?></textarea>
    <?php if(isset($errors["comment"])) { echo $errors["comment"];} ?>
    <br>
    <input type="submit" value="登録" name="submit">
  </form>
</body>
</html>

