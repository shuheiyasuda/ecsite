<?php
  session_start();
  require "validation.php";
  if(isset($_POST["submit"])) {

    $cls = new register();
    $product_errors = $cls->product_register();

    if(empty($product_errors)){
      $tempfile = $_FILES["image"]["tmp_name"];
      $filemove = '/Applications/MAMP/htdocs/ec_site/img/'. $_FILES["image"]["name"];
      move_uploaded_file($tempfile, $filemove);

      $filemove = "/ec_site/img/". $_FILES["image"]["name"];

      $item = htmlspecialchars($_POST["item"], ENT_QUOTES);
      $intro = htmlspecialchars($_POST["intro"], ENT_QUOTES);
      $price = htmlspecialchars($_POST["price"], ENT_QUOTES);

      $_SESSION["item"] = $item;
      $_SESSION["image"] = $filemove;
      $_SESSION["intro"] = $intro;
      $_SESSION["price"] = $price;
      header("Location:product_confirm.php");
    } 
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>商品登録画面</title>
</head>
<body>
  <form action="product_register.php" method="post" enctype="multipart/form-data">
    <p>商品名</p>
    <input type="text" name="item" value="<?php if(isset($_POST['item'])) { echo $_POST['item'];} ?>">
    <?php if(isset($product_errors["item"])) { echo $product_errors["item"];} ?>
    <p>商品画像</p>
    <input type="file" name="image">
    <?php if(isset($product_errors["image"])) { echo $product_errors["image"];} ?>
    <p>紹介文</p>
    <input type="text" name="intro" value="<?php if(isset($_POST['intro'])) { echo $_POST['intro'];} ?>">
    <?php if(isset($product_errors["intro"])) { echo $product_errors["intro"];} ?>
    <p>価格</p>
    <input type="text" name="price" value="<?php if(isset($_POST['price'])) { echo $_POST['price'];} ?>">
    <?php if(isset($product_errors["price"])) { echo $product_errors["price"];} ?>
    <br>
    <input type="submit" name="submit" value="送信">
  </form>
</body>
</html>