<?php

  session_start();
  $dns = ("mysql:host=localhost;dbname=ec_site;charaset=utf8");
  $user = "root";
  $password = "root"; 

  try{
    $db = new PDO($dns, $user, $password);
  } catch (PDOException $e){
    echo "接続失敗:" .$e->getMessage(). "\n";
    exit();
  }

  $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  $sql = "SELECT * FROM products";
  $stmt = $db->prepare($sql);
  $stmt->execute();
  $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
  
  //新着商品の取得
  $sql = "SELECT id,item FROM products WHERE id = ( SELECT MAX(id) FROM products)";
  $stmt = $db->prepare($sql);
  $stmt->execute();
  $new_item = $stmt->fetch(PDO::FETCH_ASSOC);

  if(isset($_POST["logout"])) {

    $_SESSION = array();
    header("Location:login.php");
  }

  if(isset($_POST["detail"])) {
    $_SESSION["item_id"] = $_POST["id"];
    header("Location:product_detail.php");
  }

  if(isset($_POST["show_cart"])) {
    header("Location:cart.php");
  }

  if(isset($_POST["rank"])) {
    header("Location:ranking.php");
  }

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>商品一覧画面</title>
</head>
<body>

  <span>新着情報！</span>
    <?php echo $new_item["item"]; ?>
    <div style="display:inline-flex">
      <form action="product_list.php" method="post">
        <input type="hidden" name="id" value="<?php echo $new_item['id']; ?>">
        <input type="submit" name="detail" value="詳細ページへ">
      </form>
    </div>

  <p><?php if(isset($_SESSION["user_name"])) { echo "ユーザー名:". $_SESSION["user_name"];} ?></p>

  <form action="product_list.php" method="post">
    <input type="submit" value="ログアウト" name="logout">
  </form>

  <form action="product_list.php" method="post">
    <input type="submit" value="カートの中身を見る" name="show_cart">
  </form>

  <form action="ranking.php" method="post">
    <input type="submit" name="rank" value="ランキング一覧画面">
  </form>

  <table border=1>
    <tr>
      <th>商品名</th>
      <th>商品画像</th>
      <th>紹介文</th>
      <th>価格</th>
    </tr>
    <?php foreach($items as $item) { ?>
      <tr>
        <td><?php echo $item["item"]; ?></td>
        <td><img src="<?php echo $item['image']; ?>"></td>
        <td><?php echo $item["intro"]; ?></td>
        <td><?php echo $item["price"]; ?></td>
        <td>
          <form action="product_list.php" method="post">
            <input type="hidden" name="id" value="<?php echo $item['id']; ?>">
            <input type="submit" value="詳細" name="detail">
          </form>
        </td>
      </tr>
    <?php } ?>
  </table>
</body>
</html>