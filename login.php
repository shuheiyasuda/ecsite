<?php
require "validation.php";
session_start();
// $_SESSION = array();
// session_destroy();
// var_dump($_SESSION["cart"]);
if(isset($_POST["submit"])){

  $cls = new login();
  $login_valid = $cls->user_login();

  if(empty($login_valid)){
    $dns = ("mysql:host=localhost;dbname=ec_site;charaset=utf8");
    $user = "root";
    $password = "root"; 
  
    try{
      $db = new PDO($dns, $user, $password);
    } catch (PDOException $e){
      echo "接続失敗:" .$e->getMessage(). "\n";
      exit();
    }
  
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
    $email = htmlspecialchars($_POST["email"], ENT_QUOTES);
    $password = htmlspecialchars($_POST["password"], ENT_QUOTES);
  
    $sql = "SELECT * FROM users where email = :email";
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':email', $email, PDO::PARAM_STR);
    $stmt->execute();
    $user = $stmt->fetch(PDO::FETCH_ASSOC);
    
    if(!empty($user)) {
      if(password_verify($password, $user["password"])) {
        $_SESSION["user_name"] = $user["name"];
        $_SESSION["user_id"] = $user["id"];
        $_SESSION["user_address"] = $user["address"];
        header("Location:product_list.php");
      } else{ 
        $login_error["password"] = "パスワードが間違っています";
      }
    } else {
      $login_error["email"] = "メールアドレスが間違っています";
    }
  } 
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ログインページ</title>
</head>
<body>
  <form action="login.php" method="post">
  <p>メールアドレス</p>
  <input type="text" name="email" value="<?php if(isset($_POST['email'])) {echo $_POST['email'];} ?>">
  <?php if(isset($login_valid["email"])) { echo $login_valid["email"]; } ?>
  <?php if(isset($login_error["email"])) { echo $login_error["email"]; } ?>
  <p>パスワード</p>
  <input type="text" name="password">
  <?php if(isset($login_valid["password"])) { echo $login_valid["password"]; } ?>
  <?php if(isset($login_error["password"])) { echo $login_error["password"]; } ?>
  <br>
  <input type="submit" name="submit" value="ログイン">
  </form>
  <a href="user_register.php">ユーザー登録画面へ</a>
</body>
</html>