<?php
  session_start();
  $dns = ("mysql:host=localhost;dbname=ec_site;charaset=utf8");
  $user = "root";
  $password = "root"; 
  
  try{
    $db = new PDO($dns, $user, $password);
  } catch (PDOException $e){
    echo "接続失敗:" .$e->getMessage(). "\n";
    exit();
  }
  
  $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


  $sql = "SELECT product_id, SUM(amount) FROM order_detail GROUP BY product_id ORDER BY SUM(amount) DESC";

  $stmt = $db->prepare($sql);
  $stmt->execute();
  
  $orders = $stmt->fetchAll(PDO::FETCH_ASSOC);
  // var_dump($orders);

  $sql = "SELECT * FROM products WHERE id = :id";
  $stmt = $db->prepare($sql);
  foreach($orders as $key => $order){
    $stmt->bindParam(":id", $order["product_id"], PDO::PARAM_STR);
    $stmt->execute();
    $items[] = $stmt->fetch(PDO::FETCH_ASSOC);
  }

  // var_dump($item);

  if(isset($_POST["back"])) {
    header("Location:product_list.php");
  }

  if(isset($_POST["detail"])) {
    $_SESSION["item_id"] = $_POST["id"];
    header("Location:product_detail.php");
  }

  $i = 0;

  ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ランキング一覧画面</title>
</head>
<body>
  <form action="ranking.php" method="post">
    <input type="submit" name="back" value="戻る">
  </form>
  <table border=1>
    <tr>
      <th>ランキング順位</th>
      <th>商品名</th>
      <th>商品画像</th>
      <th>価格</th>
      <th>商品紹介コメント</th>
    </tr>
    <?php foreach($items as $key => $item) {?>
      <tr>
        <td><?php echo $i += 1; ?></td>
        <td><?php echo $item["item"]; ?></td>
        <td><img src="<?php echo $item['image']; ?>"></td>
        <td><?php echo $item["price"]; ?></td>
        <td><?php echo $item["intro"]; ?></td>
        <td>
          <form action="ranking.php" method="post">
            <input type="hidden" name="id" value="<?php echo $item['id']; ?>">
            <input type="submit" name="detail" value="詳細ページへ">
          </form>
        </td>
      </tr>
    <?php } ?>
  </table>
</body>
</html>