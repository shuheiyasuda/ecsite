<?php
  session_start();
  require "validation.php";
  if(isset($_POST["submit"])) {

    $cls = new register();
    $errors = $cls->user_register();

    if(empty($errors)){
      $name = htmlspecialchars($_POST["name"], ENT_QUOTES);
      $post_number = htmlspecialchars($_POST["zip21"], ENT_QUOTES);
      $address = htmlspecialchars($_POST["addr21"], ENT_QUOTES);
      $email = htmlspecialchars($_POST["email"], ENT_QUOTES);
      $password = htmlspecialchars($_POST["password"], ENT_QUOTES);

      $_SESSION["name"] = $name;
      $_SESSION["post_number"] = $post_number;
      $_SESSION["address"] = $address;
      $_SESSION["email"] = $email;
      $_SESSION["password"] = $password;

      header("Location:user_confirm.php");
    }
  }
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ユーザー登録画面</title>
  <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
</head>
<body>
  <form action="user_register.php" method="post">
    <p>氏名</p>
    <input type="text" name="name" value="<?php if(isset($_POST['name'])) {echo $_POST['name'];} ?>">
    <?php if(isset($errors["name"])){ echo $errors["name"];} ?>

    <p>郵便番号</p>
    <input type="text" name="zip21" size="4" maxlength="3"> － <input type="text" name="zip22" size="5" maxlength="4" onKeyUp="AjaxZip3.zip2addr('zip21','zip22','addr21','addr21');">
    <?php if(isset($errors["post_number"])){ echo $errors["post_number"];} ?>

    <p>住所</p> 
    <input type="text" name="addr21" size="40">
    <?php if(isset($errors["address"])){ echo $errors["address"];} ?>

    <p>メールアドレス</p>
    <input type="text" name="email" value="<?php if(isset($_POST['email'])) {echo $_POST['email'];} ?>">
    <?php if(isset($errors["email"])){ echo $errors["email"];} ?>
    <p>パスワード</p>
    <input type="text" name="password" value="<?php if(isset($_POST['password'])) {echo $_POST['password'];} ?>">
    <?php if(isset($errors["password"])){ echo $errors["password"];} ?>
    <br>
    <input type="submit" value="送信" name="submit">
  </form>
</body>
</html>