<?php

session_start();
$dns = ("mysql:host=localhost;dbname=ec_site;charaset=utf8");
$user = "root";
$password = "root"; 

try{
  $db = new PDO($dns, $user, $password);
} catch (PDOException $e){
  echo "接続失敗:" .$e->getMessage(). "\n";
  exit();
}

$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$id = $_SESSION["item_id"];

$sql = "SELECT * FROM products WHERE id = :id";

$stmt = $db->prepare($sql);
$stmt->bindParam(':id', $id, PDO::PARAM_STR);
$stmt->execute();
$item = $stmt->fetch(PDO::FETCH_ASSOC);




if(isset($_POST["into_cart"])) {
  $amount = (int)$_POST["amount"];

  if(!empty($_SESSION["cart"])) {
    foreach($_SESSION["cart"] as &$cart) {
      if($item["id"] == $cart["id"]){
        $cart["amount"] += $amount;
        break;
      } else {
        $item["amount"] = $amount;
        $_SESSION["cart"][] = $item;
        break;
      }
    }
  } 

    if(empty($_SESSION["cart"])) {
      $item["amount"] = $amount;
      $_SESSION["cart"][] = $item;
    }
  header("Location:product_list.php");
}






if(isset($_POST["back"])) {
  header("Location:product_list.php");
}

if(isset($_POST["comment"])) {
  if(!isset($_SESSION["user_id"])) {
    echo "a";
    header("Location:login.php");
  } else {
    header("Location:comment_register.php");
  }
}

//コメント取得
$sql = "SELECT * FROM comments WHERE product_id = :id order by id DESC";

$stmt = $db->prepare($sql);
$stmt->bindParam(":id", $item["id"], PDO::PARAM_STR);
$stmt->execute();
$comments = $stmt->fetchAll(PDO::FETCH_ASSOC);

//削除
if(isset($_POST["delete"])) {

  $item_id = htmlspecialchars($_POST["id"]);

  $sql = "DELETE FROM comments WHERE id = :id";
  $stmt = $db->prepare($sql);
  $stmt->bindParam(":id", $item_id, PDO::PARAM_STR);
  $stmt->execute();
  header("Location:product_detail.php");
}

if(isset($_POST["good"])){

  //favoritesテーブルに同じuser_id,product_idが存在しているか調べる  
  $sql = "SELECT * FROM favorites WHERE user_id = $_SESSION[user_id] AND product_id = $item[id]";
  $stmt = $db->prepare($sql);
  $stmt->execute();
  $favorites = $stmt->fetch(PDO::FETCH_ASSOC);

  //favoritesテーブルに同じuser_id,product_idが存在していない場合(未いいね)=>追加
  if(empty($favorites)){
    $sql = "INSERT INTO favorites(id, user_id, product_id) VALUE(NULL, :user_id, :product_id)";
    $stmt = $db->prepare($sql);
    $stmt->bindParam(":user_id", $_SESSION["user_id"], PDO::PARAM_STR);
    $stmt->bindParam(":product_id", $item["id"], PDO::PARAM_STR);
    $stmt->execute();
  } else {
  //favoritesテーブルに同じuser_id,product_idが存在している場合(既いいね)=>削除
    $sql = "DELETE FROM favorites WHERE user_id = $_SESSION[user_id] AND product_id = $item[id]";
    $stmt = $db->prepare($sql);
    $stmt->execute();
  }
}

//いいね総数
  $sql = "SELECT product_id, COUNT(product_id) FROM favorites WHERE product_id = $item[id] GROUP BY product_id"; 

  $stmt = $db->prepare($sql);
  $stmt->execute();
  $favorite_count = $stmt->fetch(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>商品詳細ページ</title>
<body>
  <form action="product_detail.php" method="post">
    <input type="submit" name="back" value="戻る">
  </form>
  <form action="product_detail.php" method="post">
    <input type="submit" value="カートに追加" name="into_cart">
  <table border=1>
    <tr>
      <th>商品名</th>
      <th>商品画像</th>
      <th>紹介文</th>
      <th>価格</th>
      <th>数量</th>
    </tr>
    <tr>
      <td><?php echo $item["item"]; ?></td>
      <td><img src="<?php echo $item['image'] ?>"></td>
      <td><?php echo $item["intro"]; ?></td>
      <td><?php echo $item["price"]; ?></td>
      <td><select name="amount">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></td>
    </tr>
  </form>
  </table>

  <?php 
    if(!empty($_SESSION["user_id"])) {
      echo "<form action='product_detail.php' method='post'>";
      echo "<input type='submit' value='いいね' name='good'>";
      echo "</form>";
      if(!empty($favorite_count)) {
        echo "現在のいいね数→".$favorite_count["COUNT(product_id)"];
      }
    }
  ?>

  <p>口コミ一覧</p>
  <?php 
    if(!empty($comments)) {
      foreach($comments as $key => $comment) {
        echo $comment["nickname"]. " : " .$comment["comment"];
        if(isset($_SESSION["user_id"])){
          if($_SESSION["user_id"] == $comment["user_id"]) {
            echo "<form action='product_detail.php' method='post'>";
            echo "<input type='hidden' name='id' value='$comment[id]'>";
            echo "<input type='submit' name='delete' value='削除'>";
            echo "</form>";
          } 
        }
        echo "<br>";
      }
    } else {
        echo "口コミはまだありません";
    }
  ?>
  <form action="product_detail.php" method="post">
    <input type="submit" name="comment" value="口コミ登録">
  </form>
</body>
</html>