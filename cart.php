<?php

session_start();

// $_SESSION["cart"] = [];exit;
$dns = ("mysql:host=localhost;dbname=ec_site;charaset=utf8");
$user = "root";
$password = "root"; 

try{
  $db = new PDO($dns, $user, $password);
} catch (PDOException $e){
  echo "接続失敗:" .$e->getMessage(). "\n";
  exit();
}

$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$id = $_SESSION["item_id"];

$sql = "SELECT * FROM products WHERE id = :id";

$stmt = $db->prepare($sql);
$stmt->bindParam(':id', $id, PDO::PARAM_STR);
$stmt->execute();
$item = $stmt->fetch(PDO::FETCH_ASSOC);

//合計金額
$total = 0;
if(!empty($_SESSION["cart"])){
  foreach($_SESSION["cart"] as $cart) {
    $total += $cart["price"] * $cart["amount"];
  }
  $_SESSION["total"] = $total;
}


//カートを全て空にする
if(isset($_POST["all_delete"])) {
  unset($_SESSION["cart"]);
  $total = 0;
  if(!empty($_SESSION["cart"])){
    foreach($_SESSION["cart"] as $cart) {
      $total += $cart["price"] * $cart["amount"];
    }
    $_SESSION["total"] = $total;
  }

}

//指定した数だけ削除
if(isset($_POST["delete"])) {
  $delete_amount = (int)$_POST["amount"];
  $item_id = $_POST["item_id"];

  foreach($_SESSION["cart"] as $key => &$cart) {
    if($cart["id"] == $item_id) { 
      $cart["amount"] -= $delete_amount;
      $_SESSION["total"] = $cart["price"] * $cart["amount"];
    break;
    }  
  }

}

//選択された商品を全て削除
  
  if(isset($_POST["item_delete"])) { 
    $item_id = $_POST["item_id"];
    foreach($_SESSION["cart"] as $key => $cart) {
      if($cart["id"] == $item_id){
        unset($_SESSION["cart"][$key]);
      }
    }
    $total = 0;
    foreach($_SESSION["cart"] as $cart) {
      $total += $cart["price"] * $cart["amount"];
    }
    $_SESSION["total"] = $total;
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>カート一覧画面</title>
</head>
<body>
  <?php if(!empty($_SESSION["cart"])){ ?>
    <p>カート全体の合計金額:<?php echo $_SESSION["total"]."円"; ?></p>
    <input type="button" value="戻る" onclick="history.back()">
    <input type="button" value="購入手続きに進む" onclick="location.href='order_confirm.php'">
    <form action="cart.php" method="post">
      <input type="hidden" value="<?php echo $item['id'] ?>" name="item_id">
      <input type="submit" name="all_delete" value="カートの中身を全て削除">
    </form>
    <table border=1>
      <tr>
        <th>商品名</th>
        <th>商品画像</th>
        <th>紹介文</th>
        <th>価格</th>
        <th>数量</th>
      </tr>
      <?php foreach($_SESSION['cart'] as $item) { ?>
        <tr>
          <td><?php echo $item["item"] ?></td>
          <td><img src="<?php echo $item['image'] ?>"></td>
          <td><?php echo $item["intro"] ?></td>
          <td><?php echo $item["price"] ?></td>
          <td><?php echo $item["amount"] ?></td>
          <form action="cart.php" method="post">
            <td>
              <input type="hidden" value="<?php echo $item['id'] ?>" name="item_id">
              <input type="submit" value="全て削除" name="item_delete">
            </td>
          </form>
        </tr>
      <?php } ?>  
    </table>
  <?php } else {?>
    カートは空です。
    <a href="product_list.php">商品一覧画面へ</a>
<?php } ?>
</body>
</html>